import Vue from 'vue'
import App from './App.vue'
import Sidebar from './Sidebar.vue'
import Onboarding from './Onboarding.vue'
require("style-loader!raw-loader!./assets/animate.css");

new Vue({
    el: '#app',
    components: {
        'sidebar': Sidebar,
        'onboarding': Onboarding
    }
})